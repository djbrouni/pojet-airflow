import requests
import pymongo
import datetime
import time


profile = requests.api.get('https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=cccdcab7ae82fbfca895133844016df6').json()
rating = requests.api.get('https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=cccdcab7ae82fbfca895133844016df6').json()

data = {
        'profile': profile,
        'rating': rating,
        'timestamp': datetime.datetime.now().timestamp() 
}

print("--------------PROFIL-----------------")
print (data['profile'])
print("--------------RATING-----------------")
print (data['rating'])
print("--------------Timestamp-----------------")
print (time.strftime('%A, %Y-%m-%d %H:%M:%S', time.localtime(data['timestamp'])))


myclient = pymongo.MongoClient("mongodb://root:password@localhost:27017/")
mydb = myclient["database"]
mycol = mydb["column"]


x = mycol.insert_one(data)

print(x.inserted_id)